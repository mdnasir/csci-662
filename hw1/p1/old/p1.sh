#!/usr/bin/env bash
#script for hw1, problem 1
#creates fsa with all valid words from spanish vocab

inputfile=smallvocab.txt

while read -r line
do
	word="$line"
	wordLength=`wc -w <<< $line`
	for i in $(seq 1 $wordLength)
	do
		echo $word | cut -f$i -d' '
	done 
	
done < $inputfile
