import glob
import sys, os
import copy, pdb
import numpy as np





#inputFile = open(sys.argv[1],'r')
inputFile=open('spanishvocab.txt', 'r')

inputString = inputFile.read()
vocab = inputString.splitlines()
cleanVocab=[word.replace(" ","") for word in vocab]
wordCount=len(cleanVocab)


trList=[]
#trList=np.array(trList)
#(0 (1 "he"))
#(1 (2 "saw"))
outputFileName='spanish.fsa'
outfile = open(outputFileName,'w')

#create initial state
#outfile.write("0\n")
stateCtr=0

for index, word in enumerate(cleanVocab):

#check if the epsilon case
	if index<wordCount-1:
		nextWord=cleanVocab[index+1]
		if len(word) < len(nextWord):
			if word==nextWord[0:len(word)]:
				epsFlag=True

	if index==0:
		tr0=0
		tr=[tr0]
		for letter in word:
			stateCtr=stateCtr+1
			tr.append(stateCtr)
			outfile.write('('+str(tr[-2])+' ('+str(tr[-1]) + ' "' + letter +'"))\n')
		if epsFlag:
			stateCtr=stateCtr+1
			tr.append(stateCtr)
			outfile.write('('+str(tr[-2])+' ('+str(tr[-1]) + ' ' + '*e*' +'))\n')
			epsFlag=False
		finalState=stateCtr
		trList.append(tr)


	else:
		prevWord=cleanVocab[index-1]
		L=min(len(word), len(prevWord))
		diffC = [x!=y for (x,y) in zip(word[0:L], prevWord[0:L])]
		firstDiff=L

		for i,v in enumerate(diffC):
			if v:
				firstDiff=i
				break

		tr = trList[index-1][0:firstDiff+1]
		
		for ii,letter in enumerate(word[firstDiff:len(word)]):
			if ii==len(word[firstDiff:len(word)])-1:

				if epsFlag:
					stateCtr=stateCtr+1
					tr.append(stateCtr)
					outfile.write('('+str(tr[-2])+' ('+str(tr[-1]) + ' "' +  letter +'"))\n')
					epsFlag=False
					tr.append(finalState)
					outfile.write('('+str(tr[-2])+' ('+str(tr[-1]) + ' ' +  '*e*' +'))\n')
				else:
					tr.append(finalState)					
					outfile.write('('+str(tr[-2])+' ('+str(tr[-1]) + ' "' +  letter +'"))\n')
				

			else:
				stateCtr=stateCtr+1
				tr.append(stateCtr)
				outfile.write('('+str(tr[-2])+' ('+str(tr[-1]) + ' "' +  letter +'"))\n')
		trList.append(tr)


outfile.write('('+str(finalState)+' ('+  '0' + ' "' +  '_' +'"))\n')
#outfile.write('('+str(finalState)+')')
outfile.close()
os.system('echo "' +str(finalState)+'" | cat - '+ outputFileName +' > temp && mv temp '+ outputFileName)
