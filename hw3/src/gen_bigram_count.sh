file=../hw3/english.data

cat $file | tr ' ' '_' | grep -o . > b.tmp
cat b.tmp|  tail -n +2 > bi1.tmp
cat b.tmp | head -n -1 > bi2.tmp
paste -d'/' bi1.tmp bi2.tmp | sort | uniq -c > eng_LM_bigram.count
sort b.tmp | uniq -c > eng_LM_unigram.count
rm b*.tmp
