import glob
import sys, os, csv
import copy, pdb
from itertools import product
import numpy as np

maxIter=5


def writeOutput(EP, JP, A,outF ):
	for line in A:
			outF.write('"' + ('" "').join(EP) + '"\n')
			outF.write('"' + ('" "').join(JP) + '"\n')
			line=[ str(nn) for nn in line]
			outF.write((' ').join(line) + '\n')

def writeOutput2(EP, JP, A,outF ):
	line=A
	outF.write('"' + ('" "').join(EP) + '"\n')
	outF.write('"' + ('" "').join(JP) + '"\n')
	line=[ str(nn) for nn in line]
	outF.write((' ').join(line) + '\n')

inputFile='../hw3/epron-jpron-unsupervised.data'

outputFile='../hw3/epron-jpron-aligned-first5.data'

reader = csv.reader(open(inputFile), delimiter=" ")
glist=list(reader)

EPronList=glist[0::3]
JPronList=glist[1::3]
AlignList=[]
outF=open(outputFile, "w")





for ii, EPron in enumerate(EPronList):
	JPron = JPronList[ii]
	N=len(EPron)
	M=len(JPron)

	alphabet=range(N)
	Align = map(list, product(alphabet, repeat=M))
	Align = np.array(Align)

	delList=[]
	for i,aa in enumerate(Align):
		if np.any(np.diff(np.reshape(aa,[1,M]), axis=1)<0) or len(set(aa)) != N :
			delList.append(i)
	AlignF = np.delete(Align, delList, axis=0)
	AlignF=AlignF+1
	AlignList.append(AlignF)
	writeOutput(EPron, JPron, AlignF, outF)

outF.close()

# EM

for iterNo in range(maxIter):

	print("new Iter")

	resultsFile=open('../hw3/epron-jpron-results.data', "w")
	Plist=[]
	BestAlign=[]

	if iterNo==0:
		CTable={}
		for ii, EPron in enumerate(EPronList):
			JPron = JPronList[ii]
			AlignF= list(AlignList[ii])

			N=len(EPron)
			M=len(JPron)
			for epI, ep in enumerate(EPron):				
				for alI,align in enumerate(AlignF):
					strl='.'.join([JPron[kk] for kk, xx in enumerate(align) if int(xx)-1==epI])
					keyy= ep + '_' +strl
					if keyy not in CTable:
						CTable[keyy] = 0
					# if ep not in ETable:
					# 	ETable[ep] = 0 

#---2:
	print("Step 2 Started")


	for ii, EPron in enumerate(EPronList):
		JPron = JPronList[ii]

	#---2a
		AlignF= list(AlignList[ii])

		N=len(EPron)
		M=len(JPron)
		#pdb.set_trace()

		#Update Count Tables for ek_J and ek


	#---2b
		if iterNo==0:	
			Pl= np.array([1/float(len(AlignF))]*len(AlignF))
			Pl=np.asarray(Pl, dtype=np.float)
			Plist.append(Pl)
#			pdb.set_trace()
		else:
			Pl= np.array([1]*len(AlignF))
			Pl=np.asarray(Pl, dtype=np.float)

#			pdb.set_trace()
			
			for i,align in enumerate(AlignF):
				for k, ep in enumerate(EPron):
	#				pdb.set_trace()

					strl='.'.join([JPron[kk] for kk, xx in enumerate(align) if int(xx)-1==k])
					keyy= ep + '_' +strl

					prob =   CTable[keyy]
					if prob==0:
						
						print "what!!"
						pdb.set_trace()
					Pl[i] = float(Pl[i])*prob
			Pl=Pl/np.sum(Pl)

		Plist.append(Pl)
		yy=np.argmax(Pl)
		BestAlign.append(AlignF[yy])
		writeOutput2(EPron, JPron, AlignF[yy], resultsFile)
		print iterNo, ii


	#---2c
		for i,align in enumerate(AlignF):
			for epI, ep in enumerate(EPron):
				strl='.'.join([JPron[kk] for kk, xx in enumerate(align) if int(xx)-1==epI])
				keyy= ep + '_' +strl
				CTable[keyy] += Pl[i]
#				ETable[ep] +=Pl[i]
	
	ETable={}
	for keyy in CTable:
		keyEp = keyy.split('_')[0]
		if keyEp not in ETable:
			ETable[keyEp]=CTable[keyy]
		else:
			ETable[keyEp] += CTable[keyy]


	for keyy in CTable:
		keyEp = keyy.split('_')[0]
		if ETable[keyEp]==0:
			CTable[keyy]=0
			print "who"
		else:
			CTable[keyy]=CTable[keyy]/float(ETable[keyEp])



	resultsFile.close()

		





		












