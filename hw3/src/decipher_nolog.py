import glob
import sys, os, csv
import copy, pdb
from itertools import product
import numpy as np

def ForwardBackwardAlgo(obsSeq, hStates, p0, Tx, E):
	n=len(obsSeq)
	m=len(hStates)
	alpha=np.zeros((n,m)).astype(float)
	beta = np.zeros((n,m)).astype(float)

	for i in range(n):
		if i==0:
			for j in range(m):
				alpha[i,j]=p0[j]
		else:
			ind=hStates.index(obsSeq[i-1])
			for j in range(m):
				for k in range(m):
					new=Tx[j,k]*E[ind,k]*alpha[i-1,k]            #bigramDict[hStates[j]+'/'+hStates[k]]
					alpha[i,j] = alpha[i,j] + new

	pdata = 0
	for j in range(m):
		pdata = pdata + alpha[-1,j]

	for i in reversed(range(n)):
		if i==n-1:
			for j in range(m):
				beta[i,j]=-1
		else:
			ind=hStates.index(obsSeq[i-1])
			for j in range(m):
				for k in range(m):
					new=Tx[k,j] * E[ind,k] * beta[i+1,k]            #bigramDict[hStates[j]+'/'+hStates[k]]
					beta[i,j] = beta[i,j] + new
	return alpha, beta, pdata


def ViterbiSearch(obsSeq, hStates, p0, Tx, E):
	n=len(obsSeq)
	m=len(hStates)
	Q=np.zeros((n,m)).astype(float)
	bestPred=np.zeros((n,m))

	for i in range(n):
		ind=hStates.index(obsSeq[0])
		if i==0:
			for j in range(m):
				Q[i,j]=p0[j] *E[ind,j]
		else:
			for j in range(m):
				Q[i,j] = 0
				rr=np.array([0.]*m)
				for k in range(m):
					rr[k] = Tx[j,k] * E[ind,j] *Q[i-1,k]
				bestPred[i,j] = np.argmax(rr)
				Q[i,j] = np.max(rr)

	finalBest=np.argmax(Q[-1,:])
	finalScore = np.max(Q[-1,:])
	decoded = hStates[finalBest]

	current=int(finalBest)
	for i in reversed(range(n-1)):
		current =  int(bestPred[i+1, current])
		decoded = hStates[current] + decoded
	return decoded
#################################################################### Main


# load cipher
inputFile1='../hw3/cipher.data'
#reader = csv.reader(open(inputFile1), delimiter=" ")
#cipher=list(reader)
#cipher = [item for sublist in cipher for item in sublist]
cipher = open(inputFile1,'r').read().replace(' ' , '_').replace('\n','')
cipher=cipher


#load LM counts and get LM probs
#---------unigram
unigramFile='eng_LM_unigram.count'
reader = csv.reader(open(unigramFile), delimiter=" ")
UniList=list(reader)
Alphabet=list(range(len(UniList)))
UniCount=list(range(len(UniList)))
for i,line in enumerate(UniList):
	Alphabet[i] =line[-1]
	UniCount[i]=int(line[-2])
UniCount=np.array(UniCount).astype(float)
UniProb=[0.]*UniCount.shape[0]
uninorm=sum(UniCount)
for i in range(len(UniProb)):
	UniProb[i]=float(UniCount[i])/uninorm     #add-one smoothing for unigram

#---------bigram
biFile='eng_LM_bigram.count'
reader = csv.reader(open(biFile), delimiter=" ")
BiList=list(reader)
obsBi=list(range(len(BiList)))
BiCount=list(range(len(BiList)))
for i,line in enumerate(BiList):
	obsBi[i] =line[-1]
	BiCount[i]=int(line[-2])


V=len(Alphabet)
bigramDict={}
Combi = map(list, product(Alphabet, repeat=2))
for pair in Combi:
	keyy=pair[0]+'/'+pair[1]
	if keyy in obsBi:
		cc=BiCount[obsBi.index(keyy)]
	else:
		cc=0
	bigramDict[keyy] = float(cc +1)/float(UniCount[Alphabet.index(pair[1])] + V)


# EM training-------------------------------------

# transition probablities
TxProb=np.zeros((V,V)).astype(float)
for i in range(len(Alphabet)):
	for j in range(len(Alphabet)):
		t_i = Alphabet[i]
		t_j = Alphabet[j]
		keyy = t_i + '/' +t_j
		TxProb[i,j] = bigramDict[keyy]

# initialize emission probabilities
e2cProb=np.zeros((V,V)).astype(float)

#iterations
maxIter=3
for iterNo in range(maxIter):
	
	if iterNo==0:
		for k in range(V):
			for i in range(V):
				obs = Alphabet[i]
				tag = Alphabet[k]
				if tag == '_':
					if obs=='_':
						e2cProb[i,k]=1
					else:
						e2cProb[i,k]=0
				else:
					e2cProb[i,k] = 1/float(V)
		
	#initial probabilities
	P_i=np.array([0.]*V)
	for i in range(V):
		space_ind=Alphabet.index('_')
		P_i[i]=TxProb[i,space_ind]
	#pdb.set_trace()
	Alpha,Beta,pp=ForwardBackwardAlgo(cipher, Alphabet, P_i, TxProb, e2cProb)

	e2cCount = np.zeros((V,V)).astype(float)
	for i in range(len(cipher)):
		for k in range(V):
			new=Alpha[i,k] * e2cProb[Alphabet.index(cipher[i]),k] * Beta[i,k]-pp
			e2cCount[Alphabet.index(cipher[i]),k] = e2cCount[Alphabet.index(cipher[i]),k] + new

	for k in range(V):
		for i in range(V):
			obs = Alphabet[i]
			tag = Alphabet[k]
			if tag == '_':
				if obs=='_':
					e2cProb[i,k]=1
				else:
					e2cProb[i,k]=0
	pdb.set_trace()

	for k in range(V):
		total=0
		for i in range(V):
			total=total + e2cCount[i,k]
		for i in range(V):
			e2cProb[i,k] = e2cCount[i,k]/total

	print pp
	decoded= ViterbiSearch(cipher, Alphabet, P_i, TxProb, e2cProb)
	print decoded




















		













