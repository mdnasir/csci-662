



def alignPron(EPron,JPron)
	N=len(EPron)
	M=len(JPron)

	alphabet=range(N)
	Align = map(list, product(alphabet, repeat=M))
	Align = np.array(Align)

	delList=[]
	for i,aa in enumerate(Align):
		if np.any(np.diff(np.reshape(aa,[1,M]), axis=1)<0) or len(set(aa)) != N :
			delList.append(i)
	AlignF = np.delete(Align, delList, axis=0)
	AlignF=AlignF+1
	return AlignF