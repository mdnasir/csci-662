import glob
import sys, os, csv
import copy, pdb
from itertools import product
import numpy as np
import matplotlib.pyplot as plt




def addlog(x, y):
	if x == -np.inf:
		return y
	if y == -np.inf:
		return x
	if x-y > 16:
		return x
	if x > y:
		return (x + np.log(1 + np.exp(y-x)))
	if y-x > 16:
		return y
	if y >= x:
		return (y + np.log(1 + np.exp(x-y)))


def ForwardBackwardAlgo(obsSeq, hStates, p0, Tx, E):

	n=len(obsSeq)
	m=len(hStates)
	alpha=np.zeros((n,m)).astype(float)
	beta = np.zeros((n,m)).astype(float)
	alpha[:]=-np.inf
	beta[:]=-np.inf
	for i in range(n):
		if i==0:
			for j in range(m):
				alpha[i,j]=p0[j]+E[obsSeq[i],j]
		else:
			for j in range(m):
				for k in range(m):
					new=Tx[j,k] + E[obsSeq[i],j] + alpha[i-1,k]            #bigramDict[hStates[j]+'/'+hStates[k]]
					alpha[i,j] = addlog(alpha[i,j], new)

	pdata = -np.inf
	for j in range(m):
		pdata = addlog(pdata, alpha[-1,j])

	for i in reversed(range(n)):
		if i==n-1:
			for j in range(m):
				beta[i,j]=0
		else:
			for j in range(m):
				for k in range(m):
					new=Tx[k,j] + E[obsSeq[i+1],k] + beta[i+1,k]            #bigramDict[hStates[j]+'/'+hStates[k]]
					beta[i,j] = addlog(beta[i,j], new)

#	pdb.set_trace()
	absum=[]
	for i in range(len(obsSeq)):
		total=-np.inf
		for j in range(m):
			total = addlog(total, alpha[i,j] + beta[i,j])
		absum.append(total)

	#print np.diff(np.array(absum))
	#pdb.set_trace()
	return alpha, beta, absum[-2]


def ViterbiSearch(obsSeq, hStates, p0, Tx, E):
	n=len(obsSeq)
	m=len(hStates)
	Q=np.zeros((n,m)).astype(float)
	Q[:]=-np.inf
	bestPred=np.zeros((n,m))

	for i in range(n):	
		if i==0:
			for j in range(m):
				Q[i,j]=p0[j] + E[obsSeq[i],j]
		else:
			for j in range(m):
				Q[i,j] = -np.inf 
				rr=np.array([0.]*m)
				for k in range(m):
					rr[k] = Tx[j,k] + E[obsSeq[i],j] +Q[i-1,k]
				bestPred[i,j] = np.argmax(rr)
				Q[i,j] = np.max(rr)

	finalBest=np.argmax(Q[-1,:])
	finalScore = np.max(Q[-1,:])
	decoded = hStates[finalBest]

	current=int(finalBest)
	for i in reversed(range(n-1)):
		current =  int(bestPred[i+1, current])
		decoded = hStates[current] + decoded
	return decoded
#################################################################### Main
cNo=-1
maxIter=100

# load cipher
inputFile1='../hw3/cipher_new.data'
#reader = csv.reader(open(inputFile1), delimiter=" ")
#cipher=list(reader)
#cipher = [item for sublist in cipher for item in sublist]
cipher = open(inputFile1,'r').read().replace(' ' , '_').replace('\n','')
cipher=cipher[0:cNo]


#load LM counts and get LM probs
#---------unigram
unigramFile='eng_LM_unigram.count'
reader = csv.reader(open(unigramFile), delimiter=" ")
UniList=list(reader)
Alphabet=list(range(len(UniList)))
UniCount=list(range(len(UniList)))
for i,line in enumerate(UniList):
	Alphabet[i] =line[-1]
	UniCount[i]=int(line[-2])
UniCount=np.array(UniCount).astype(float)
UniProb=[0.]*UniCount.shape[0]
uninorm=sum(UniCount)
for i in range(len(UniProb)):
	UniProb[i]=np.log(UniCount[i]/uninorm)     #add-one smoothing for unigram

#---------bigram
biFile='eng_LM_bigram.count'
reader = csv.reader(open(biFile), delimiter=" ")
BiList=list(reader)
obsBi=list(range(len(BiList)))
BiCount=list(range(len(BiList)))
for i,line in enumerate(BiList):
	obsBi[i] =line[-1]
	BiCount[i]=int(line[-2])


V=len(Alphabet)
bigramDict={}
Combi = map(list, product(Alphabet, repeat=2))
for pair in Combi:
	keyy=pair[0]+'/'+pair[1]
	if keyy in obsBi:
		cc=BiCount[obsBi.index(keyy)]
	else:
		cc=0
	bigramDict[keyy] = np.log(float(cc +1)/float(UniCount[Alphabet.index(pair[1])] + V))
	#bigramDict[keyy] = np.log(float(cc)/float(UniCount[Alphabet.index(pair[1])]))

# EM training-------------------------------------

# transition probablities
TxProb=np.zeros((V,V)).astype(float)
for i in range(len(Alphabet)):
	for j in range(len(Alphabet)):
		t_i = Alphabet[i]
		t_j = Alphabet[j]
		keyy = t_i + '/' +t_j
		TxProb[i,j] = bigramDict[keyy]

# initialize emission probabilities
e2cProb=np.zeros((V,V)).astype(float)

#iterations

probData=[]
for iterNo in range(maxIter):
	
	if iterNo==0:
		for k in range(V):
			for i in range(V):
				obs = Alphabet[i]
				tag = Alphabet[k]
				if tag == '_':
					if obs=='_':
						e2cProb[i,k]=0
					else:
						e2cProb[i,k]=-np.inf
				else:
					e2cProb[i,k] = np.log(1/float(V))
		
	#initial probabilities
	P_i=np.array([0.]*V)
	space_ind=Alphabet.index('_')
	for i in range(V):
		P_i[i]=TxProb[i,space_ind]
	#pdb.set_trace()
	cipherID=[]
	for c in cipher:
		cipherID.append(Alphabet.index(c))
	if iterNo>0:
		pp_prev=pp
	Alpha,Beta,pp=ForwardBackwardAlgo(cipherID, Alphabet, P_i, TxProb, e2cProb)

	#debug----------------------------------------------------------------
# 	absum=[]
# 	for i in range(len(cipher)):
# 		total=-np.inf
# 		for j in range(V):
# 			total = addlog(total, Alpha[i,j] + Beta[i,j])
# 		absum.append(total)
# #	print absum


	e2cCount = np.zeros((V,V)).astype(float)
	for i,c_id in enumerate(cipherID):
		for k in range(V):
			#new=Alpha[i,k] + e2cProb[Alphabet.index(cipher[i]),k] + Beta[i,k]-pp
			new=Alpha[i,k] + Beta[i,k]-pp
			e2cCount[c_id,k] = addlog(e2cCount[c_id,k], new)



	for k in range(V):
		total=-np.inf
		for i in range(V):
			total=addlog(total, e2cCount[i,k])
		for i in range(V):
			e2cProb[i,k] = e2cCount[i,k] - total

	for k in range(V):
		for i in range(V):
			obs = Alphabet[i]
			tag = Alphabet[k]
			if tag == '_' and obs=='_':
				e2cProb[i,k]=0
			elif (tag == '_' and obs!='_') or (tag != '_' and obs=='_'):
				e2cProb[i,k]=-np.inf

	if iterNo>0:
		print iterNo, pp, np.exp(float(pp-pp_prev))
	else:
		print pp
	probData.append(pp)
	decoded= ViterbiSearch(cipherID, Alphabet, P_i, TxProb, e2cProb)
	print decoded



print probData
plt.plot(np.array(probData))
plt.xlabel('iteration')
plt.ylabel('log P(cipher|model)')
#plt.title('')
plt.grid(True)
plt.show()

















		













