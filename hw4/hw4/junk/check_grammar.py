#!/usr/bin/env python

import sys, fileinput,os
import tree,pdb, copy
import bigfloat as bf
import numpy as np
from time import time
import math
import matplotlib.pyplot as plt


case_sense=1
vert_mark=0
horz_mark=1
part='test'
parseInputfile=part+'.strings'
parseOutputfile=part+'.parses'


def addtoDict(Dict,item):
	if item in Dict:
		Dict[item]+=1
	else:
		Dict[item]=1
	return Dict

def union(a, b):
    return list(set(a) | set(b))

def extract(back,X, i,j):
	if j-i > 1:
		rule=back[i][j][X]
		Y=rule.split(' -> ')[1].split('-')[0]
		Z=rule.split(' -> ')[1].split(' ')[1].split('-')[0]
		k=int(rule.split(' -> ')[1].split(' ')[0].split(',')[-1])
		return union([ back[i][j][X] ], union(extract(back,Y,i,k),extract(back,Z,k,j)) )
	else:
		return [back[i][j][X]]

def getParse(nodekey,G):
	for rule in G:
		if nodekey in rule:
			if rule.count(' ')==2:
				X=rule.split(' -> ')[0].split('-')[0]
				w=rule.split(' -> ')[1].split('-')[0]
				return ' ('+ X + ' ' + w + ')'
			if rule.count(' ')==3:
				X=rule.split(' -> ')[0].split('-')[0]
				Ykey=rule.split(' -> ')[1].split(' ')[0] + ' -> '
				Zkey=rule.split(' -> ')[1].split(' ')[1] + ' -> '
				if X=='TOP':
					return '('+ X + getParse(Ykey,G) + getParse(Zkey,G) + ')'
				else:
					return ' ('+ X + getParse(Ykey,G) + getParse(Zkey,G) + ')'

def remove_mom(parseIn,symb):
	parseOut=[]
	nodeL=parseIn.split(' ')
	for nd in nodeL:
		parseOut.append(nd.split(symb)[0])
	return (' ').join(parseOut)

def func(x,k):
	return pow(x,k)

gf=open('grammar.txt','w')


ParentDict={}
RuleDict={}
wordset=[]
tagset=[]
bottomRules={}
NTRules={}
noDerivations=[]
for line in fileinput.input():
# lines=["(TOP (SQ (VBZ Does) (SQ* (NP (DT that) (NN flight)) (VP (VB serve) (NP_NN dinner)))) (PUNC ?))\n"]
# for line in lines:
	line=line.strip('\n')
	all_nodes=line.split(' ')
	for i,node in enumerate(all_nodes):
		next_nodes=all_nodes[i+1:]
		count=0
		childset=[]
		childind=[]
		if node[-1]==')':
			continue
			######   cannot be a parent
		parent=node.strip('(')
		if parent not in tagset:
			tagset.append(parent)
		for j,next in enumerate(next_nodes):
			if j==0 and next[-1]==')':
				if case_sense==1:
					child=next.strip(')')
				else:
					child=next.strip(')').lower()
				childset=[child]
				if child not in wordset:
					wordset.append(child)
				bottomRules=addtoDict(bottomRules, parent + ' -> ' + child)
				break
				#  found its only child
			if next[0]=='(':
				if count==0 and len(childset)<2:
					if vert_mark==1 and horz_mark==0:
						childset.append(next.strip('(').split(':')[0]+':'+parent.split(':')[0].split('~')[0])
					elif vert_mark==0 and horz_mark==1 and len(childset)==1:
						if *' in parent or '~' in parent:
							childset.append(next.strip('(').split('~')[0]+'~'+childset[0].split(':')[0].split('~')[0])
						else:
							childset.append(next.strip('('))
					elif vert_mark==1 and horz_mark==1 and len(childset)==1:
						if *' in parent or '~' in parent:
							childset.append(next.strip('(').split(':')[0]+':'+parent.split(':')[0]+'~'+childset[0].split(':')[0].split('~')[0])
						else:
							childset.append(next.strip('(').split(':')[0]+':'+parent.split(':')[0].split('~')[0])
					elif vert_mark==1 and horz_mark==1 and len(childset)==0:
						childset.append(next.strip('(').split(':')[0]+':'+parent.split(':')[0].split('~')[0])

					else:
						childset.append(next.strip('('))
						
					childind.append(i+j+1)
					if vert_mark==1 and horz_mark==0:
						all_nodes[i+j+1]=all_nodes[i+j+1].split(':')[0]+':'+parent.split(':')[0].split('~')[0]
					if vert_mark==0 and horz_mark==1 and len(childset)==2:
						if *' in parent or '~' in parent:
							all_nodes[i+j+1]=all_nodes[i+j+1].split('~')[0]+'~'+childset[0].split(':')[0].split('~')[0]
					if vert_mark==1 and horz_mark==1 and len(childset)==2:
						if *' in parent or '~' in parent:
							all_nodes[i+j+1]=all_nodes[i+j+1].split(':')[0]+':'+parent.split(':')[0]+'~'+childset[0].split(':')[0].split('~')[0]
						else:
							all_nodes[i+j+1]=all_nodes[i+j+1].split(':')[0]+':'+parent.split(':')[0].split('~')[0]
					if vert_mark==1 and horz_mark==1 and len(childset)==1:
						all_nodes[i+j+1]=all_nodes[i+j+1].split(':')[0]+':'+parent.split(':')[0].split('~')[0]
				count+=1
			count-=next.count(')')
		ParentDict=addtoDict(ParentDict,parent)
		chstr=' '.join(childset)
		rule=parent+' -> '+chstr
		RuleDict=addtoDict(RuleDict,rule)


ProbRuleDict={}
maxCountRule='x'
maxCount=0
for rule in RuleDict:
	parent=rule.split(' -> ',1)[0]
	x=RuleDict[rule]
	y=ParentDict[parent]
	prob=float(x)/float(y)
	ProbRuleDict[rule]=prob
	if x > maxCount:
		maxCountRule=rule
		maxCount=x
	gf.write(rule + ' # ' + str(prob)+'\n')

gf.close()

for key in bottomRules:
	bottomRules[key]=ProbRuleDict[key]
for key,val in ProbRuleDict.iteritems():
	if key not in bottomRules:
		NTRules[key]=val

TagDict1={}
for tag in tagset:
	TagDict1[tag]=0
TagDict2={}
for tag in tagset:
	TagDict2[tag]=[]

print len(ProbRuleDict)
print len(tagset)