#!/usr/bin/env python

import sys, fileinput,os
import tree,pdb, copy
import bigfloat as bf
import numpy as np
from time import time
import math
import matplotlib.pyplot as plt


case_sense=1
vert_mark=0
horz_mark=0
part='test'
parseInputfile=part+'.strings'
parseOutputfile=part+'.parses'


def addtoDict(Dict,item):
	if item in Dict:
		Dict[item]+=1
	else:
		Dict[item]=1
	return Dict

def union(a, b):
    return list(set(a) | set(b))

def extract(back,X, i,j):
	if j-i > 1:
		rule=back[i][j][X]
		Y=rule.split(' -> ')[1].split('-')[0]
		Z=rule.split(' -> ')[1].split(' ')[1].split('-')[0]
		k=int(rule.split(' -> ')[1].split(' ')[0].split(',')[-1])
		return union([ back[i][j][X] ], union(extract(back,Y,i,k),extract(back,Z,k,j)) )
	else:
		return [back[i][j][X]]

def getParse(nodekey,G):
	for rule in G:
		if nodekey in rule:
			if rule.count(' ')==2:
				X=rule.split(' -> ')[0].split('-')[0]
				w=rule.split(' -> ')[1].split('-')[0]
				return ' ('+ X + ' ' + w + ')'
			if rule.count(' ')==3:
				X=rule.split(' -> ')[0].split('-')[0]
				Ykey=rule.split(' -> ')[1].split(' ')[0] + ' -> '
				Zkey=rule.split(' -> ')[1].split(' ')[1] + ' -> '
				if X=='TOP':
					return '('+ X + getParse(Ykey,G) + getParse(Zkey,G) + ')'
				else:
					return ' ('+ X + getParse(Ykey,G) + getParse(Zkey,G) + ')'

def remove_mom(parseIn,symb):
	parseOut=[]
	nodeL=parseIn.split(' ')
	for nd in nodeL:
		parseOut.append(nd.split(symb)[0])
	return (' ').join(parseOut)

def func(x,k):
	return pow(x,k)

gf=open('grammar.txt','w')


ParentDict={}
RuleDict={}
wordset=[]
tagset=[]
bottomRules={}
NTRules={}
noDerivations=[]
for line in fileinput.input():
# lines=["(TOP (SQ (VBZ Does) (SQ* (NP (DT that) (NN flight)) (VP (VB serve) (NP_NN dinner)))) (PUNC ?))\n"]
# for line in lines:
	line=line.strip('\n')
	all_nodes=line.split(' ')
	for i,node in enumerate(all_nodes):
		next_nodes=all_nodes[i+1:]
		count=0
		childset=[]
		childind=[]
		if node[-1]==')':
			continue
			######   cannot be a parent
		parent=node.strip('(')
		if parent not in tagset:
			tagset.append(parent)
		for j,next in enumerate(next_nodes):
			if j==0 and next[-1]==')':
				if case_sense==1:
					child=next.strip(')')
				else:
					child=next.strip(')').lower()
				childset=[child]
				if child not in wordset:
					wordset.append(child)
				bottomRules=addtoDict(bottomRules, parent + ' -> ' + child)
				break
				#  found its only child
			if next[0]=='(':
				if count==0 and len(childset)<2:
					if vert_mark==1 and horz_mark==0:
						childset.append(next.strip('(').split(':')[0]+':'+parent.split(':')[0].split('~')[0])
					elif vert_mark==0 and horz_mark==1 and len(childset)==1:
						if '*' in parent:
							childset.append(next.strip('(').split('~')[0]+'~'+childset[0].split(':')[0].split('~')[0])
						else:
							childset.append(next.strip('('))
					elif vert_mark==1 and horz_mark==1 and len(childset)==1:
						if '*' in parent:
							childset.append(next.strip('(').split(':')[0]+':'+parent.split(':')[0]+'~'+childset[0].split(':')[0].split('~')[0])
						else:
							childset.append(next.strip('(').split(':')[0]+':'+parent.split(':')[0].split('~')[0])
					elif vert_mark==1 and horz_mark==1 and len(childset)==0:
						childset.append(next.strip('(').split(':')[0]+':'+parent.split(':')[0].split('~')[0])

					else:
						childset.append(next.strip('('))
						
					childind.append(i+j+1)
					if vert_mark==1 and horz_mark==0:
						all_nodes[i+j+1]=all_nodes[i+j+1].split(':')[0]+':'+parent.split(':')[0].split('~')[0]
					if vert_mark==0 and horz_mark==1 and len(childset)==2:
						if '*' in parent:
							all_nodes[i+j+1]=all_nodes[i+j+1].split('~')[0]+'~'+childset[0].split(':')[0].split('~')[0]
					if vert_mark==1 and horz_mark==1 and len(childset)==2:
						if '*' in parent:
							all_nodes[i+j+1]=all_nodes[i+j+1].split(':')[0]+':'+parent.split(':')[0]+'~'+childset[0].split(':')[0].split('~')[0]
						else:
							all_nodes[i+j+1]=all_nodes[i+j+1].split(':')[0]+':'+parent.split(':')[0].split('~')[0]
					if vert_mark==1 and horz_mark==1 and len(childset)==1:
						all_nodes[i+j+1]=all_nodes[i+j+1].split(':')[0]+':'+parent.split(':')[0].split('~')[0]
				count+=1
			count-=next.count(')')
		ParentDict=addtoDict(ParentDict,parent)
		chstr=' '.join(childset)
		rule=parent+' -> '+chstr
		RuleDict=addtoDict(RuleDict,rule)


ProbRuleDict={}
maxCountRule='x'
maxCount=0
for rule in RuleDict:
	parent=rule.split(' -> ',1)[0]
	x=RuleDict[rule]
	y=ParentDict[parent]
	prob=float(x)/float(y)
	ProbRuleDict[rule]=prob
	if x > maxCount:
		maxCountRule=rule
		maxCount=x
	gf.write(rule + ' # ' + str(prob)+'\n')

gf.close()

for key in bottomRules:
	bottomRules[key]=ProbRuleDict[key]
for key,val in ProbRuleDict.iteritems():
	if key not in bottomRules:
		NTRules[key]=val

TagDict1={}
for tag in tagset:
	TagDict1[tag]=0
TagDict2={}
for tag in tagset:
	TagDict2[tag]=[]
##########################  PARSING############################
devC=0
nilC=0
lenList=[]
timeList=[]
pf=open(parseOutputfile,'w')
Lines=open(parseInputfile).read().splitlines()
for line in Lines:
	t0=time()
	tokens=line.split(' ')
	if case_sense==0:
		tokens=[tok.lower() for tok in tokens]
	n=len(tokens)
	for i0, word in enumerate(tokens):
		if word not in wordset:
			tokens[i0]='<unk>'

	best=[]
	back=[]
	for ii in range(n):             #i=0..n-1   in py, 0..n-1  # j=1..n   in py, 0..n-1
		row=[]
		rowback=[]
		for jj in range(n+1):
			if jj>ii:			  
				ch1=copy.deepcopy(TagDict1)
				ch2=copy.deepcopy(TagDict2)
			else:
				ch1=[]
				ch2=[]
			row.append(ch1)
			rowback.append(ch2)
		best.append(row)
		back.append(rowback)

	bestA=np.array(best)

	for i in range(1,n+1):
		word=tokens[i-1]
		for rule,p in bottomRules.iteritems():
			if word in rule:
				X=rule.split(' -> ',1)[0]
				if p>best[i-1][i][X]:
					best[i-1][i][X]=p
					back[i-1][i][X]=X+'-' + str(i-1) +','+str(i)+' -> ' + word + '-' +str(i) 
	for l in range(2,n+1):
		for i in range(n-l+1):
			j=i+l
			for k in range(i+1,j):
				for rule,p in NTRules.iteritems():
					X=rule.split(' -> ',1)[0]
					Y=rule.split(' -> ',1)[1].split(' ')[0]
					Z=rule.split(' -> ',1)[1].split(' ')[1]
					pp=bf.bigfloat(p)*bf.bigfloat(best[i][k][Y])
					pp=pp*bf.bigfloat(best[k][j][Z])
					if pp>bf.bigfloat(best[i][j][X]):
						best[i][j][X]=pp
						back[i][j][X] = X+'-' + str(i) +','+str(j)+' -> ' + Y + '-' +str(i) +','+ str(k) + \
						' ' + Z + '-'+ str(k) +','+ str(j)
	if back[0][n]['TOP']:
		GG=extract(back, 'TOP', 0 ,n)
		topkey='TOP'+'-0,'+str(n)
		parse=getParse(topkey,GG)
		if vert_mark==1:
			parse=remove_mom(parse, ':')
		if horz_mark==1:
			parse=remove_mom(parse, '~')
		pf.write(parse+'\n')
	else:
		pf.write('nil\n')
		nilC+=1
		noDerivations.append(devC+1)
	if devC==0 and part=='dev':
		if back[0][n]['TOP']:
			print math.log10(float(best[0][8]['TOP']))
			print parse
		else:
			print "No derivation found"

	devC+=1
	t1=time()
	lenList.append(n)
	timeList.append(t1-t0)

#	print('dev sentence done:'+str(devC))

pf.close()

print noDerivations
cmd1='python postprocess.py ' + parseOutputfile +' > ' + parseOutputfile +'.post'
cmd2 = 'python evalb.py '+ parseOutputfile +'.post' +' ' + part +  '.trees'
os.system(cmd1)
os.system(cmd2)





