cut -f2 -d'/' train-data | tail -n +2 > bi1.tmp
cut -f2 -d'/' train-data | head -n -1 > bi2.tmp
paste -d'/' bi1.tmp bi2.tmp | sort | uniq -c > POS_LM_bigram.count
sort bi2.tmp | uniq -c > POS_LM_bigram.norm
