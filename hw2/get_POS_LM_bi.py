import glob
import sys, os, csv
import copy, pdb



inputFile='files/POS_LM_unigram.count'
reader = csv.reader(open(inputFile), delimiter=" ")
Uglist=list(reader)
Uclist=list(range(len(Uglist)))

for i,line in enumerate(Uglist):
	Uglist[i]=line[-1]
	Uclist[i]=int(line[-2])

stateList=list(range(len(Uglist)))
finalState=Uglist.index('.')

outputFileName='bigram.wfsa'
outfile = open(outputFileName,'w')
#create initial state
outfile.write(str(finalState)+"\n")



#inputFile = open(sys.argv[1],'r')
inputFile='files/POS_LM_bigram.count'
reader = csv.reader(open(inputFile), delimiter=" ")
glist=list(reader)
clist=list(range(len(glist)))
#Plist=list(range(len(glist)))

for i,line in enumerate(glist):
	glist[i]=line[-1]
	clist[i]=int(line[-2])

for jj, ss in enumerate(glist):
	st1=Uglist.index(ss.split('/')[1])
	st2=Uglist.index(ss.split('/')[0])
	totCnt=Uclist[st1]
	prob=clist[jj]/totCnt
	outline='(' + str(st1) + ' (' + str(st2) + ' ' + '"' + ss.split('/')[0] +'" ' + str(prob) + '))\n'
#	print(outline)
	outfile.write(outline)

outfile.close()


