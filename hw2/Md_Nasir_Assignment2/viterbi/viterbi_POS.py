import glob
import sys, os, csv
import copy, pdb
import numpy as np


testFileName='../../files/test-data-1.sent'


######  tag to word

inputFile='files/train-data.count'

reader = csv.reader(open(inputFile), delimiter=" ")
tag2wordlist=list(reader)
clist=list(range(len(tag2wordlist)))
tag2word=list(range(len(tag2wordlist)))
wordlist=list(range(len(tag2wordlist)))
Ptag2word=list(range(len(tag2wordlist)))

for i,line in enumerate(tag2wordlist):
	tag2word[i]=line[-1]
	tag2wordlist[i]=line[-1].split('/')
	wordlist[i]=tag2wordlist[i][0]
	clist[i]=int(line[-2])


Uwordlist=sorted(set(wordlist))


Ptag2word=list(np.loadtxt('files/tag2wordProb.txt'))


T2Wzip=zip(tag2word, Ptag2word)
tag2wordDict={}
for trans, prob in T2Wzip:
	tag2wordDict[trans]=float(prob)



######## load bigram tag LM

inputFile='files/POS_LM_unigram.count'
reader = csv.reader(open(inputFile), delimiter=" ")
Uglist=list(reader)
Uclist=list(range(len(Uglist)))

for i,line in enumerate(Uglist):
	Uglist[i]=line[-1]
	Uclist[i]=int(line[-2])

stateList=list(range(len(Uglist)))
finalState=Uglist.index('.')


inputFile='files/POS_LM_bigram.count'
reader = csv.reader(open(inputFile), delimiter=" ")
glist=list(reader)
clist=list(range(len(glist)))
Pbigram=list(range(len(glist)))

for i,line in enumerate(glist):
	glist[i]=line[-1]
	clist[i]=int(line[-2])

for jj, ss in enumerate(glist):
	st1=Uglist.index(ss.split('/')[1])
	st2=Uglist.index(ss.split('/')[0])
	totCnt=Uclist[st1]
	prob=float(clist[jj])/float(totCnt)
	Pbigram[jj]=prob


bigramzip=zip(glist, Pbigram)
bigramDict={}
for trans, prob in bigramzip:
	bigramDict[trans]=float(prob)



###

testF=open(testFileName, 'r')
testSen=testF.read().split('\n')



###  Viterbi algorithm -------------------------------------------------------------------------
# unique words: WordU
# unique tags: TagU



TagU=Uglist
WordU=Uwordlist
m=len(TagU)


num=0
den=0


for ii,sen in enumerate(testSen):

	if not sen:
		break
	sen=sen[1:-1]
	testWords= sen.split('" "')
	n=len(testWords)
	Q=np.zeros((n, m))
	bestPred=np.zeros((n, m),dtype=np.int)

	decoded=[]

	for j in range(m):
		tag=TagU[j]
		t2t = tag +"/."
		if t2t in bigramDict:
			P_tag = bigramDict[t2t]
		else:
			P_tag=0
		word=testWords[0]
		t2w=word + '/' + tag
		if t2w in tag2wordDict:
			P_w=tag2wordDict[t2w]
		else:
			P_w=0
		Q[0,j]=P_tag*P_w


	for i in range(n):
		word=testWords[i]
		if i==0:
			continue
		for j in range(m):
			tag=TagU[j]
			bestScore=float('-inf')
			
			t2w=word + '/' + tag
			if t2w in tag2wordDict:
				P_w=tag2wordDict[t2w]
			else:
				P_w=0

			for k in range(m):
				t2t=tag + "/"+ TagU[k]
				if t2t in bigramDict:
					P_tag = bigramDict[t2t]
				else:
					P_tag=0
				r=P_tag*P_w*Q[i-1,k]
				if r>bestScore:
					bestScore=r
					bestPred[i,j]=k
					Q[i,j]=r

	num=num+float(np.count_nonzero(Q))
	den=den+float(m*n)

	finalBest=0
	finalScore=float('-inf')
	for j in range(m):
		if Q[-1,j] > finalScore:
			finalScore =Q[-1,j]
			finalBest=j

	decoded.append(TagU[finalBest])
	current=finalBest
	for ll in range(n-1):
		i=n-2-ll
		current=bestPred[i+1,current]
		decoded.append(TagU[current])
	decoded.reverse()
	decoded=' '.join(decoded)
	print(decoded)
	if ii==0:
		Qsave=Q[0:2,:]



PctNzQ=100*num/den
print('\n')
print('Percentage of nonzero entries in Q is '+str(PctNzQ))






















