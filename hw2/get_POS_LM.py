import glob
import sys, os, csv
import copy, pdb
#inputFile = open(sys.argv[1],'r')
inputFile='files/POS_LM_unigram.count'

outputFileName='unigram.wfsa'
outfile = open(outputFileName,'w')
#create initial state
outfile.write("0\n")


reader = csv.reader(open(inputFile), delimiter=" ")
glist=list(reader)
clist=list(range(len(glist)))
#Plist=list(range(len(glist)))

for i,line in enumerate(glist):
	glist[i]=line[-1]
	clist[i]=int(line[-2])
totCnt=sum(clist)
for jj, ss in enumerate(glist):
	prob=clist[jj]/totCnt
	outline='(0 (0 ' + '"' + ss +'" ' + str(prob) + '))\n'
#	print(outline)
	outfile.write(outline)

outfile.close()


