import glob
import sys, os, csv
import copy, pdb
#inputFile = open(sys.argv[1],'r')
inputFile='files/epron-jpron.data'

reader = csv.reader(open(inputFile), delimiter=" ")
glist=list(reader)


outputFileName='epron-jpron.wfst'
outfile = open(outputFileName,'w')

ephnlist=[]
jphnlist=[]
strlist=[]

for ind, ephn in enumerate(glist):
 	if ind%3==0:
 		jphn=glist[ind+1]
 		align=glist[ind+2]
 		for ii, ep in enumerate(ephn):
 			ephnlist.append(ep)
 #			jphnlist.append([jphn[kk] for kk, xx in enumerate(align) if int(xx)-1==ii])
 			strl='.'.join([jphn[kk] for kk, xx in enumerate(align) if int(xx)-1==ii])
 			strl=ep+'_'+strl
 			strlist.append(strl)

strlistU=sorted(list(set(strlist)))

Plist=[]
trList=[]


for jj, ss in enumerate(strlistU):
	Cnt=strlist.count(ss)
	totCnt=ephnlist.count(ss.split('_')[0])
	prob=Cnt/totCnt
	if prob>=0.01:
		Plist.append(prob)
		trList.append(ss)


#create initial state
outfile.write("0\n")
stateCtr=0

for kk, ss in enumerate(trList):
	inPhn=ss.split('_')[0]
	outPhn=ss.split('_')[1]
	outL=len(outPhn.split('.'))
	if outL==1:
		outline='(0 (0 ' + '"' +  inPhn+ '" "'+outPhn+'" ' + str(Plist[kk]) + '))\n'
		print(outline)
		outfile.write(outline)
	else:
		for ll, cc in enumerate(outPhn.split('.')):
			if ll==0:
				stateCtr=stateCtr+1
				outline='(0 (' + str(stateCtr) +' "' +  inPhn+ '" "'+cc+'" ' + str(Plist[kk]) + '))\n'
				print(outline)
				outfile.write(outline)
			elif ll==outL-1:
				outline='(' + str(stateCtr) +' (0 ' +  '*e*'+ ' "'+cc+'" ' + '1.0' + '))\n'
				print(outline)
				outfile.write(outline)
			else:
				stateCtr=stateCtr+1
				outline='(' + str(stateCtr-1) +' (' + str(stateCtr) +' ' +  '*e*'+ ' "'+cc+'" ' + '1.0' + '))\n'
				print(outline)
				outfile.write(outline)

outfile.close()

